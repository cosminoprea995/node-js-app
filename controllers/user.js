const User = require('../models').User;
// const Classroom = require('../models').Classroom;
// const Course = require('../models').Course;

module.exports = {
    list(req, res) {
        return User
            .findAll()
            .then((users) => res.status(200).send(users))
            .catch((error) => {
                res.status(400).send(error);
            });
    },
    login(req, res) {
        return User
            .findAll()
            .then((users) => {
                let filteredUsers = users.filter(user => {
                    console.log(req.body.username);
                    console.log(user.username);
                    console.log(req.body.password);
                    console.log(user.password);
                    return user.username === req.body.username && user.password === req.body.password;
                });
                if (filteredUsers.length === 0) {
                    return res.status(404).send({
                        message: 'Login failed',
                    });
                }
                return res.status(200).send(filteredUsers[0]);
            })
            .catch((error) => res.status(400).send(error));
    },
    getById(req, res) {
        return User
            .findById(req.params.id, {})
            .then((user) => {
                if (!user) {
                    return res.status(404).send({
                        message: 'User  Not Found',
                    });
                }
                return res.status(200).send(user);
            })
            .catch((error) => res.status(400).send(error));
    },

    add(req, res) {
        User.findAll()
            .then((users) => {

                let duplicateUser = users.filter(user => {
                    console.log(req.body.username);
                    console.log(user.username);
                    console.log(req.body.password);
                    console.log(user.password);
                    return user.username === req.body.username;
                }).length;
                if (duplicateUser > 0) {
                    res.status(400).send("User duplicated");
                    return null;
                } else {
                    return User
                        .create({
                            id: req.body.id,
                            username: req.body.username,
                            password: req.body.password,
                            firstName: req.body.firstName,
                            lastName: req.body.lastName,
                        })
                        .then((user) => res.status(201).send(user))
                        .catch((error) => res.status(400).send(error));
                }

            })
            .catch((error) => {
                res.status(400).send(error);
            });


        // validation


    },

    // addCourse(req, res) {
    //   return Student
    //     .findById(req.body.student_id, {
    //       include: [{
    //         model: Classroom,
    //         as: 'classroom'
    //       },{
    //         model: Course,
    //         as: 'courses'
    //       }],
    //     })
    //     .then((student) => {
    //       if (!student) {
    //         return res.status(404).send({
    //           message: 'Student Not Found',
    //         });
    //       }
    //       Course.findById(req.body.course_id).then((course) => {
    //         if (!course) {
    //           return res.status(404).send({
    //             message: 'Course Not Found',
    //           });
    //         }
    //         student.addCourse(course);
    //         return res.status(200).send(student);
    //       })
    //     })
    //     .catch((error) => res.status(400).send(error));
    // },

    update(req, res) {
        return User
            .findById(req.params.id, {
                include: [],
            })
            .then(user => {
                if (!user) {
                    return res.status(404).send({
                        message: 'User Not Found',
                    });
                }
                return user
                    .update({
                        name: req.body.name
                    })
                    .then(() => res.status(200).send(user))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },

    delete(req, res) {
        return User
            .findById(req.params.id)
            .then(user => {
                if (!user) {
                    return res.status(400).send({
                        message: 'User Not Found',
                    });
                }
                return user
                    .destroy()
                    .then(() => res.status(204).send())
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },
};
